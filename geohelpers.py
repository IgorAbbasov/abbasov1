# encoding: UTF-8
'''
Created on Jan 1, 2013

@author: obrizan
'''

import math
import sys


''' http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Python '''
def deg2num(lat_deg, lon_deg, zoom):
	lat_rad = math.radians(lat_deg)
	n = 2.0 ** zoom
	xtile = int((lon_deg + 180.0) / 360.0 * n)
	ytile = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
	return (xtile, ytile)


''' http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Python '''
def num2deg(xtile, ytile, zoom):
	n = 2.0 ** zoom
	lon_deg = xtile / n * 360.0 - 180.0
	lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n)))
	lat_deg = math.degrees(lat_rad)
	return (lat_deg, lon_deg)


# Haversine formula example in Python
# Author: Wayne Dyck
# http://webcache.googleusercontent.com/search?q=cache%3Ahttp%3A%2F%2Fwww.platoscave.net%2Fblog%2F2009%2Foct%2F5%2Fcalculate-distance-latitude-longitude-python%2F&oq=cache%3Ahttp%3A%2F%2Fwww.platoscave.net%2Fblog%2F2009%2Foct%2F5%2Fcalculate-distance-latitude-longitude-python%2F&aqs=chrome.0.57j58.2792&sugexp=chrome,mod=9&sourceid=chrome&ie=UTF-8
def distance(origin, destination):
	lat1, lon1 = origin
	lat2, lon2 = destination
	radius = 6371 # km
	
	dlat = math.radians(lat2-lat1)
	dlon = math.radians(lon2-lon1)
	a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
		* math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
	c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
	d = radius * c
	
	return d



def dist(a, b, k = 32.6086957):
	"""
	Расстояние между двумя точками
	a, b - две точки
	k - коэффициент, 32.6086957 - это расстояние в метрах между двумя тайлами 
	на зум-левеле 19 в метрах 
	"""
	return math.sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2) * k


def distToLine(p, a, b):
	"""
	Расстояние от точки p до линии, проходящей через точки a и b
	"""
	normalLength = math.hypot(b[0] - a[0], b[1] - a[1])
	return abs((p[0]-a[0])*(b[1]-a[1]) - (p[1]-a[1])*(b[0]-a[0])) / normalLength


def distSquared(a, b):
	"""
	Квадрат расстояния между точками a, b
	"""
	return (a[0]-b[0])**2 + (a[1]-b[1])**2


def distToSegmentSquared(p, a, b):
	"""
	Квадрат расстояния от точки p до отрезка (a, b)
	http://stackoverflow.com/a/1501725/124115
	"""
	l2 = distSquared(a, b)
	if l2 == 0.0:
		return distSquared(p, a)
	#   ((p.x  - v.x)  * (w.x  - v.x)  + (p.y  - v.y)  * (w.y  - v.y))  / l2;
	t = ((p[0] - a[0]) * (b[0] - a[0]) + (p[1] - a[1]) * (b[1] - a[1])) / float(l2)
	if t < 0.0:
		return distSquared(p, a)
	if t > 1.0:
		return distSquared(p, b)
	#                      v.x  + t * (w.x  - v.x)   v.y  + t * (w.y  - v.y)
	return distSquared(p, (a[0] + t * (b[0] - a[0]), a[1] + t * (b[1] - a[1])))


def distToSegment(p, a, b):
	"""
	Квадрат расстояния от точки p до отрезка (a, b)
	"""
	return math.sqrt(distToSegmentSquared(p, a, b))


def distToRoadSquared(p, segments):
	""" Кратчайшее расстояние от точки p до дороги segments.
	"""
	min_distance = sys.float_info.max
	""" Вычисляем расстояние до первого сегмента. """
	d1 = distToSegmentSquared(p, segments[0], segments[1])
	
	""" Вычисляем расстояние до последнего сегмента. """
	#segments.reverse()
	#d2 = distToSegmentSquared(p, segments[0], segments[1])
	
	""" Меняем порядок обхода точек, если начало дороги к нам ближе. """
	#if d2 > d1:
	#	segments.reverse()
	min_distance = d1
	#else:
	#	min_distance = d2
		
	""" Проходим по всем сегментам и если мы нашли минимум, то выходим из цикла. """
	for i in range(1, len(segments)-1):
		d = distToSegmentSquared(p, segments[i], segments[i+1])
		if (d < min_distance):
			min_distance = d
	#	else:
	#		break
	return min_distance


def distToNearestRoad(p, ways):
	distance = sys.float_info.max
	for way in ways:
		if len(way) > 1:
			d = distToRoadSquared(p, way)
			if d < distance:
				distance = d
	return math.sqrt(distance)


def distToRoad(p, segments):
	return math.sqrt(distToRoadSquared(p, segments))


def distanceFromRoadsMap(ways):
	print "Calculating distance to roads..."
	values = []
	ystart = 177564
	yend = 178095
	totaly = yend-ystart
	xstart = 314741
	xend = 315222
	for (index, y) in enumerate(range(ystart, yend+1)):
		print "Progress: " + str(index / float(totaly))
		row = []
		for x in range(xstart, xend+1):
			row.append(int(distToNearestRoad((x, y), ways) * 32.6086957))
		values.append(row)
	return values

