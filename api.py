# encoding: utf-8
'''
Created on 22.02.2013

@author: Павел Педай
'''
import hashlib
import webapp2
import geohelpers

from django.utils import simplejson

from myModel.PKspot import OurGeoPt
from myModel.PKspot import PKspot
from google.appengine.ext import ndb


class ObjectIdEncoder(simplejson.JSONEncoder):
    def default(self, o):
        return simplejson.JSONEncoder.default(o)


class ApiHandler(webapp2.RequestHandler):
    """
        BaseHandler for all requests

        Holds the auth and session properties so they are reachable for all requests
    """
    def dispatch(self):
        """
            Save the sessions for preservation across requests
        """
        try:
            response = super(ApiHandler, self).dispatch()
            if response is not None:
                self.response.write(simplejson.dumps(response, cls=ObjectIdEncoder, sort_keys=True))
        finally:
            self.response.headers['Content-Type'] = 'application/json'

    def handle_exception(self, exception, debug):
        # If the exception is a HTTPException, use its error code.
        # Otherwise use a generic 500 error code.
        if isinstance(exception, webapp2.HTTPException):
            self.response.set_status(exception.code)
            r = {'error': exception.detail, 'code': exception.code}
            self.response.write(simplejson.dumps(r))
        else:
            self.response.set_status(500)
            r = {'error': 'Internal Server Error', 'code': 500, 'message': exception.message}
            self.response.write(simplejson.dumps(r))


class PKspotHandler(ApiHandler):
    def get(self):
        return [pkspot.to_dict() for pkspot in PKspot.query().fetch()]
    
    def post(self):
        address = self.request.get('address')
         
        polygon = simplejson.loads(self.request.get('polygon'))
        coords = [OurGeoPt(lat=i["lat"], lon=i["lng"]) for i in polygon]
        
        pkspot = PKspot( address=address, polygon=coords)
        pkspot.put()
        
        return pkspot.to_dict()

class ParkHandler(ApiHandler):
    def delete(self, park_id):
        k = ndb.Key(urlsafe=park_id)
        k.delete()

    def put(self, park_id):
        obj = simplejson.loads(park_id)
        k = ndb.Key(urlsafe=obj['cur_id'])
        current = k.get()
        k.delete()
        current.free = int(obj['new_free'])
        current.put()
        

        

app = webapp2.WSGIApplication([
                            webapp2.Route(r'/api/pkspots', handler=PKspotHandler, name='PKspotHandler'),
							webapp2.Route(r'/api/pkspots/<park_id>', handler=ParkHandler, name='ParkHandler'),
                            ], debug=True)
