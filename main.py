# encoding: utf-8
'''
Created on 22.02.2013

@author: �������������
'''

import jinja2
import os
import webapp2


jinja_environment = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))


class MainHandler(webapp2.RequestHandler):
    def get(self):
        template = jinja_environment.get_template('/templates/index.html')
        self.response.write(template.render())


app = webapp2.WSGIApplication([
                            webapp2.Route(r'/', handler=MainHandler, name='MainHandler'),
                            ], debug=True)