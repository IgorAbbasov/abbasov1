#encoding: utf-8
'''
Created on 22.02.2013

@author: Администратор
'''

from google.appengine.ext import ndb

class OurGeoPt(ndb.Model):
	lat = ndb.FloatProperty()
	lon = ndb.FloatProperty()

class PKspot(ndb.Model):
	polygon = ndb.StructuredProperty(OurGeoPt, repeated=True)
	address = ndb.StringProperty()

	def to_dict(self):
		d = super(PKspot, self).to_dict()
		""" Pre-process datetime into strings as it is not JSON-serializable """
		d['id'] = str(self.key.urlsafe())
		return d
