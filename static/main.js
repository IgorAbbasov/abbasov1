/*_local parking model_*/
function ParkViewModel(data) {
	var self = this;
	self.address_c = ko.observable(data.address_c);
	self.polygon = ko.observable(data.polygon);
	self.id = ko.observable(data.id);
	self.layerId = ko.observable(data.layerId);
}

function PUT (data){
	var self=this;
	self.cur_id = ko.observable(data.cur_id);
	self.new_free = ko.observable(data.new_free);
}

function App() {
	var self = this;
	var flag = 0;
	var closest = "";
	var clos_line_st = 0;
	var clos_line_end =0;
	var point=0;

	self.parkings = ko.observableArray();
	self.currentParking = ko.observable();
	self.selectedValue = ko.observable();
	self.sval = ko.observable("enter address...");
	self.ch = ko.observable();

	/*_Searching single park in collection by address_*/
	self.search = function() {
		var sr = false;
		for (item in self.parkings()) {
			if (self.parkings()[item].address_c().toLowerCase().indexOf(self.sval().toLowerCase()) !== -1) {
				self.map.setView(self.parkings()[item].polygon()._latlngs[0], 12);
				self.parkings()[item].polygon().openPopup();
				sr = true;
				break;
			}
		}
		if (!sr)
			alert("No results for " + self.sval());
	}

	/*_Remove selected parking from server storage and local collection_*/
	self.removePark = function(parking) {
		$.ajax({
			url : '/api/pkspots/' + parking.id(),
			type : 'delete',
			success : function() {
				self.map.removeLayer(self.map._layers[parking.layerId()]);
				self.parkings.remove(parking);
				alert("Deleted");
			}
		});
	}

	
	

	

	/*_Creating map_*/
	self.map = L.map('map', {
		drawControl : true
	}).setView([50.0, 36.25], 12);

	L.tileLayer('http://{s}.tile.cloudmade.com/77c132cb60f9451b8cdca8aed5c70ae3/82180/256/{z}/{x}/{y}.png', {
		attribution : '<a href="#" id="attribution">Attribution</a>',
		maxZoom : 18
	}).addTo(self.map);

	/*_Catching object creating event_*/
	self.map.on('draw:created', function(e) {
		var type = e.layerType, layer = e.layer;

		if (type === 'polygon') {
			self.currentParking(new ParkViewModel({
				polygon : layer
			}));
			self.showModal();
		}
		
		if (type === 'marker'){
			point = layer.getLatLng();
			var item=0
			var cur = 5000000;
			var dist = 0;
			while (self.parkings()[item]){
				var k=0;
				for (i in self.parkings()[item].polygon()._latlngs){
					if (i==self.parkings()[item].polygon()._latlngs.length-1) 
						k = 0*1 
					else 
						k=1*i+1;
				dist = self.distTo(point, self.parkings()[item].polygon()._latlngs[i], self.parkings()[item].polygon()._latlngs[k], true )
				if (dist < cur) {
					
				cur = dist;
				clos_line_st = self.parkings()[item].polygon()._latlngs[i];
				clos_line_end = self.parkings()[item].polygon()._latlngs[k];
			}
			}
				item++
			}
			var point = L.point(self.distTo(point, clos_line_st, clos_line_end,false));
			var marker = L.marker ([point.x,point.y]);
			marker.addTo(self.map);
			alert("Проникновение")			
		}
		
	})
		
	self.distTo = function(p,p1,p2,sqDist){
		var x = p1.lat,
	    y = p1.lng,
	    dx = p2.lat - x,
	    dy = p2.lng - y,
	    dot = dx * dx + dy * dy,
	    t;
	if (dot > 0) {
		t = ((p.lat - x) * dx + (p.lng - y) * dy) / dot;
		if (t > 1) {
			x = p2.lat;
			y = p2.lng;
		} else if (t > 0) {
			x += dx * t;
			y += dy * t;
		}
	}
	dx = p.lat - x;
	dy = p.lng - y;
	return sqDist ? dx * dx + dy * dy : new L.Point(x, y);
	}
	
	self.currentMarker = null;
	self.addMode = false;

	/*_Getting all parkings from server and putting it into local collection_*/
	$.getJSON('/api/pkspots', function(spots) {
		$.map(spots, function(pkspot) {
			self.pkPush(pkspot)
		});
	});

	/*_Modal window for entering data about single parking, which is creating_*/
	self.showModal = function() {
		$(function() {
			$('#myModal').arcticmodal({
				closeOnEsc : false
			});
		});
	}

	/*_Creating parking when all necessary data is collected..._*/
	self.createParking = function(parking) {
		if (self.checkData(parking)) {
			var polygon_str = JSON.stringify(self.currentParking().polygon()._latlngs);
			/*_...store it on the server..._*/
			$.post('/api/pkspots', {
				polygon : polygon_str,
				address : self.currentParking().address_c()
			}, function(pkspot){
				/*_...receive created parking as response from server..._*/
				/*_...and store it in local collection_*/
				self.pkPush(pkspot); 
				alert("Объект охраны добавлен.")
				})
			$('#myModal').arcticmodal('close')
		}
	};

	
	/*Push parking object from server to local collection*/
	self.pkPush = function (pkspot){
		/*_Creating geopoint array for polygon_*/
		var polyspot = $.map(pkspot.polygon, function(item) {
			return new L.LatLng(item.lat, item.lon);
		});
		var poly = L.polygon(polyspot);
		poly.bindPopup("<p>" + pkspot.address + "</p>")
		poly.addTo(self.map);
		/*_Getting layerID_*/
		for (item in self.map._layers)
			lid = self.map._layers[item]._leaflet_id;
		
		/*_Store parking at local collection with all necessary parameters_*/
		self.parkings.push(new ParkViewModel({
			address_c : pkspot.address,
			polygon : poly,
			id : pkspot.id,
			layerId : lid
		}));
	}
	
	/*_Focusing map view at new parking after selectedValue being changed_*/
	self.change = function() {
		if (flag) {
			self.map.setView(self.selectedValue().polygon()._latlngs[0], 12);
			self.selectedValue().polygon().openPopup();
		}
		else
			flag = true;
	}

	/*_Data check to sure that all data are correct_*/
	self.checkData = function(parking) {
		
					return true;
				
	}
}
var app = new App();
ko.applyBindings(app);